(function($){
   "use strict";
	//----------> Site Preloader	
	$(window).load(function() {	
		if($('#preloader').length){
			$('#preloader').delay(200).fadeOut(500);
		}
	});	
	$(document).ready(function() {
		$('.search-toggle').click(function () {
			$('.search-form-block-wrapper').toggleClass('open');
		});
		$('.search-action').click(function () {
			$(this).parents('form').submit();
		})
		$('.search-close').click(function () {
			$(this).parents('.search-form-block-wrapper').removeClass('open');
		});
		/*Go to top*/
		$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
		  $('#go-to-top').css({
			bottom: "20px"
		  });
		} else {
		  $('#go-to-top').css({
			bottom: "-100px"
		  });
		}
		});
		$('#go-to-top').click(function () {
		$('html, body').animate({
		  scrollTop: '0px'
		}, 800);
		return false;
		});	
		/* Tooltips */
		if ($().tooltip) {
			$("[data-toggle='tooltip']").tooltip();
		}
		if ($('#section-page-title .inv-region').hasClass('region-banner')) {
			$('#section-page-title').addClass('has-banner');
		}
		//----------------------------------> Magnific Popup Lightbox
		if ( $.isFunction($.fn.magnificPopup) ) {
			$('.expand_image').each(function(index, element) {
			$(this).click(function() {		
				$(this).parent().find(".mf-popup").find(".mf-no-gallery a").click();
				$(this).parent().find(".mf-popup").find(".carousel .carousel-inner a:first").click();
				return false;
			});
			});
			$(".magnific-popup, a[data-rel^='magnific-popup'],.mf-no-gallery a").magnificPopup({ 
				type: 'image',
			});
			$('.mf-popup').find('.carousel .carousel-inner').once('mfp-processed').each(function() {
				$(this).magnificPopup({
					delegate: 'a',
					type: 'image',
					
					gallery: {
						enabled: true
					},
					removalDelay: 500,
					callbacks: {
						beforeOpen: function() {
							this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							this.st.mainClass = /*this.st.el.attr('data-effect')*/ "mfp-zoom-in";
						}
					},
					closeOnContentClick: true,
					// allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source
					midClick: true ,	  
					retina: {
						ratio: 1,
						replaceSrc: function(item, ratio) {
						  return item.src.replace(/\.\w+$/, function(m) { return '@2x' + m; });
						} 
					}
				  
				});
			});
		}
	});
})(window.jQuery);

