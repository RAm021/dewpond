'use strict';

var app = angular.module(
		'ezeeApp',
		[ 'LocalStorageModule', 'ngCookies', 'ngAria',
				'ngCacheBuster', 'ngMaterial', 'ngAnimate',
				'ngMessages',
				'ui.bootstrap', 'ui.router' ])
	
		
		app.controller('ezeeCtrl', function($scope,$http) {
   // $scope.d = $scope.material.t_c;
   var url = "http://www.leecostechnology.com/api/ajax_courier_calculation.php ";

   $http.post(url)
            .success(function (data) {
				alert(data);
            })
            .error(function (data) {
            });
$scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
        "rough_cutting_speed":0,
		"finish_cutting_speed":0,
        "cutting_speed_30percent":0,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
        "density":0,
        "rate":0,
		"step_buffer":1.5,
		"machinerate":400
		};
			$scope.wt = {
				"0":0,
				"51":0,
				"101":0,
				"501":0,
				"1001":0
			};
			$scope.qt = {
				"100":0,
				"301":0,
				"501":0,
				"1001":0,
				"3001":0
			};
	
	// $scope.length=10;
	// $scope.dia=5;
  $scope.calculate = function(){

	if($scope.material_selected=="Aluminum"){
alert("Aluminum");
    $scope.material={
        "t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":50,
		"finish_cutting_speed":100,
		"cutting_speed_30percent":15,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":2.7,
        "rate":250,
		"step_buffer":1.5,
		"machinerate":400
			};
			$scope.wt = {
				"0":1.8,
				"51":1.7,
				"101":1.6,
				"501":1.5,
				"1001":1.4
			};
			$scope.qt = {
				"100":1.5,
				"301":1.35,
				"501":1.25,
				"1001":1.15,
				"3001":1.05
			}
    }else if($scope.material_selected=="Mildsteel"){
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":40,
		"finish_cutting_speed":80,
		"cutting_speed_30percent":12,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":7.85,
        "rate":80,
		"step_buffer":1,
        "machinerate":400
		};	$scope.wt = {
				"0":1.5,
				"51":1.4,
				"101":1.3,
				"501":1.2,
				"1001":1.1
			};
			$scope.qt = {
				"100":1.5,
				"301":1.4,
				"501":1.3,
				"1001":1.2,
				"3001":1.1
			}
    }else if($scope.material_selected=="Stainless"){
     alert("steel");
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":30,
		"finish_cutting_speed":60,
		"cutting_speed_30percent":9,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":8,
        "rate":230,
		"step_buffer":1,
		"machinerate":400
	};
	$scope.wt = {
				"0":1.5,
				"51":1.4,
				"101":1.3,
				"501":1.2,
				"1001":1.1
			};
			$scope.qt = {
				"100":1.5,
				"301":1.4,
				"501":1.3,
				"1001":1.2,
				"3001":1.1
			}
    }else if($scope.material_selected=="Plastic"){
     alert("plastic");
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":50,
		"finish_cutting_speed":100,
		"cutting_speed_30percent":15,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":2.2,
        "rate":200,
		"step_buffer":1.5,
	    "machinerate":400		
	};
	$scope.wt = {
				"0":1.8,
				"51":1.7,
				"101":1.6,
				"501":1.5,
				"1001":1.4
			};
			$scope.qt = {
				"100":1.5,
				"301":1.35,
				"501":1.25,
				"1001":1.15,
				"3001":1.05
			};
    }else if($scope.material_selected=="Copper"){
     alert("Copper");
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":25,
		"finish_cutting_speed":50,
		"cutting_speed_30percent":7.5,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":8.96,
        "rate":700,
		"step_buffer":1,
		"machinerate":400
	};
	$scope.wt = {
				"0":1.5,
				"51":1.4,
				"101":1.3,
				"501":1.2,
				"1001":1.1
			};
			$scope.qt = {
				"100":1.5,
				"301":1.4,
				"501":1.3,
				"1001":1.2,
				"3001":1.1
			}
    }else if($scope.material_selected=="tool"){
     alert("tool");
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":30,
		"finish_cutting_speed":60,
		"cutting_speed_30percent":9,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":7.85,
        "rate":800,
		"step_buffer":1,
        "machinerate":400		
	};
	$scope.wt = {
				"a":1.5,
				"51":1.4,
				"101":1.3,
				"501":1.2,
				"1001":1.1
			};
			$scope.qt = {
				"100":1.5,
				"301":1.4,
				"501":1.3,
				"1001":1.2,
				"3001":1.1
			}
    }else if($scope.material_selected=="Brass"){
     alert("Brass");
    $scope.material={
		"t_c":10,
		"rough_dia_c":5,
        "finish_dia_c":1,
		"rough_cutting_speed":60,
		"finish_cutting_speed":120,
		"cutting_speed_30percent":18,
        "feed_rough":0.3,
		"feed_finish":0.1,
		"depth_of_cut_rough":2,
		"depth_of_cut_finish":0.25,
		"density":8.73,
        "rate":500,
		"step_buffer":1,
		"machinerate":400
	};
	$scope.wt = {
				"0":1.5,
				"51":1.4,
				"101":1.3,
				"501":1.2,
				"1001":1.1
			};
			$scope.qt = {
				"100":1.5,
				"301":1.4,
				"501":1.3,
				"1001":1.2,
				"3001":1.1
			}
    }

    	
if($scope.measurement=="inch")
    {
	
    var length = ($scope.length*25.4);
    var dia = ($scope.dia*25.4);
    var step_turning_length_1 = parseInt($scope.step_turn_length_1)*25.4;
	var step_turning_length_2 = parseInt($scope.step_turn_length_2)*25.4;
	var step_turning_length_3 = parseInt($scope.step_turn_length_3)*25.4;
	var step_turning_length_4 = parseInt($scope.step_turn_length_4)*25.4;
	var step_turning_length_5 = parseInt($scope.step_turn_length_5)*25.4;
	
    var step_turning_dia_1 = parseInt($scope.step_turn_dia_1)*25.4;
	var step_turning_dia_2 = parseInt($scope.step_turn_dia_2)*25.4;
    var step_turning_dia_3 = parseInt($scope.step_turn_dia_3)*25.4;
    var step_turning_dia_4 = parseInt($scope.step_turn_dia_4)*25.4;
    var step_turning_dia_5 = parseInt($scope.step_turn_dia_5)*25.4;
	
	//boring 
	 var boring_length_1 = parseInt($scope.boring_length_1)*25.4;
	 var boring_length_2 = parseInt($scope.boring_length_2)*25.4;
	 var boring_length_3 = parseInt($scope.boring_length_3)*25.4;
	 var boring_length_4 = parseInt($scope.boring_length_4)*25.4;
	 var boring_length_5 = parseInt($scope.boring_length_5)*25.4;
	 
	 var boring_dia_1 = parseInt($scope.boring_dia_1)*25.4;
	 var boring_dia_2 = parseInt($scope.boring_dia_2)*25.4;
	 var boring_dia_3 = parseInt($scope.boring_dia_3)*25.4;
	 var boring_dia_4 = parseInt($scope.boring_dia_4)*25.4;
	 var boring_dia_5 = parseInt($scope.boring_dia_5)*25.4;
	 
	// Internal Threading
	var Internal_threading_length_1 = parseInt($scope.Internal_threading_length_1)*25.4;
    var Internal_threading_length_2 = parseInt($scope.Internal_threading_length_2)*25.4;
	var Internal_threading_length_3 = parseInt($scope.Internal_threading_length_3)*25.4;
	var Internal_threading_length_4 = parseInt($scope.Internal_threading_length_4)*25.4;
	var Internal_threading_length_5 = parseInt($scope.Internal_threading_length_5)*25.4;
	
	var Internal_threading_dia_1 = parseInt($scope.Internal_threading_dia_1)*25.4;
    var Internal_threading_dia_2 = parseInt($scope.Internal_threading_dia_2)*25.4;
	var Internal_threading_dia_3 = parseInt($scope.Internal_threading_dia_3)*25.4;
	var Internal_threading_dia_4 = parseInt($scope.Internal_threading_dia_4)*25.4;
	var Internal_threading_dia_5 = parseInt($scope.Internal_threading_dia_5)*25.4;
	
	var Internal_threading_pitch_1 = parseInt($scope.Internal_threading_pitch_1)*25.4;
    var Internal_threading_pitch_2 = parseInt($scope.Internal_threading_pitch_2)*25.4;
	var Internal_threading_pitch_3 = parseInt($scope.Internal_threading_pitch_3)*25.4;
	var Internal_threading_pitch_4 = parseInt($scope.Internal_threading_pitch_4)*25.4;
	var Internal_threading_pitch_5 = parseInt($scope.Internal_threading_pitch_5)*25.4;
	
	
	// External Threading
	var external_threading_length_1 = parseInt($scope.external_threading_length_1)*25.4;
    var external_threading_length_2 = parseInt($scope.external_threading_length_2)*25.4;
	var external_threading_length_3 = parseInt($scope.external_threading_length_3)*25.4;
	var external_threading_length_4 = parseInt($scope.external_threading_length_4)*25.4;
	var external_threading_length_5 = parseInt($scope.external_threading_length_5)*25.4;
	
	var external_threading_dia_1 = parseInt($scope.external_threading_dia_1)*25.4;
    var external_threading_dia_2 = parseInt($scope.external_threading_dia_2)*25.4;
	var external_threading_dia_3 = parseInt($scope.external_threading_dia_3)*25.4;
	var external_threading_dia_4 = parseInt($scope.external_threading_dia_4)*25.4;
	var external_threading_dia_5 = parseInt($scope.external_threading_dia_5)*25.4;
	
	var external_threading_pitch_1 = parseInt($scope.external_threading_pitch_1)*25.4;
    var external_threading_pitch_2 = parseInt($scope.external_threading_pitch_2)*25.4;
	var external_threading_pitch_3 = parseInt($scope.external_threading_pitch_3)*25.4;
	var external_threading_pitch_4 = parseInt($scope.external_threading_pitch_4)*25.4;
	var external_threading_pitch_5 = parseInt($scope.external_threading_pitch_5)*25.4;
	
	//kurling
	var knurling_dia_1 = ($scope.knurling_dia_1)*25.4;
	var knurling_length_1 = ($scope.knurling_length_1)*25.4;
	
	console.log("length in inches: "+length);
	console.log("dia in inches: "+dia);
    }else
    {
    var length = parseInt($scope.length);
    var dia = parseInt($scope.dia);
	
    var step_turning_length_1 = parseInt($scope.step_turn_length_1);
    var step_turning_length_2 = parseInt($scope.step_turn_length_2);
	var step_turning_length_3 = parseInt($scope.step_turn_length_3);
	var step_turning_length_4 = parseInt($scope.step_turn_length_4);
	var step_turning_length_5 = parseInt($scope.step_turn_length_5);
	
    var step_turning_dia_1 = parseInt($scope.step_turn_dia_1);
	var step_turning_dia_2 = parseInt($scope.step_turn_dia_2);
    var step_turning_dia_3 = parseInt($scope.step_turn_dia_3);
    var step_turning_dia_4 = parseInt($scope.step_turn_dia_4);
    var step_turning_dia_5 = parseInt($scope.step_turn_dia_5);
	
	//boring 
	 var boring_length_1 = parseInt($scope.boring_length_1);
	 var boring_length_2 = parseInt($scope.boring_length_2);
	 var boring_length_3 = parseInt($scope.boring_length_3);
	 var boring_length_4 = parseInt($scope.boring_length_4);
	 var boring_length_5 = parseInt($scope.boring_length_5);
	 
	 var boring_dia_1 = parseInt($scope.boring_dia_1);
	 var boring_dia_2 = parseInt($scope.boring_dia_2);
	 var boring_dia_3 = parseInt($scope.boring_dia_3);
	 var boring_dia_4 = parseInt($scope.boring_dia_4);
	 var boring_dia_5 = parseInt($scope.boring_dia_5);
	 
		// Internal Threading
	var Internal_threading_length_1 = parseInt($scope.Internal_threading_length_1);
    var Internal_threading_length_2 = parseInt($scope.Internal_threading_length_2);
	var Internal_threading_length_3 = parseInt($scope.Internal_threading_length_3);
	var Internal_threading_length_4 = parseInt($scope.Internal_threading_length_4);
	var Internal_threading_length_5 = parseInt($scope.Internal_threading_length_5);
	
	var Internal_threading_dia_1 = parseInt($scope.Internal_threading_dia_1);
    var Internal_threading_dia_2 = parseInt($scope.Internal_threading_dia_2);
	var Internal_threading_dia_3 = parseInt($scope.Internal_threading_dia_3);
	var Internal_threading_dia_4 = parseInt($scope.Internal_threading_dia_4);
	var Internal_threading_dia_5 = parseInt($scope.Internal_threading_dia_5);
	
	var Internal_threading_pitch_1 = parseInt($scope.Internal_threading_pitch_1);
    var Internal_threading_pitch_2 = parseInt($scope.Internal_threading_pitch_2);
	var Internal_threading_pitch_3 = parseInt($scope.Internal_threading_pitch_3);
	var Internal_threading_pitch_4 = parseInt($scope.Internal_threading_pitch_4);
	var Internal_threading_pitch_5 = parseInt($scope.Internal_threading_pitch_5);
	
	
	// External Threading
	var external_threading_length_1 = parseInt($scope.external_threading_length_1);
    var external_threading_length_2 = parseInt($scope.external_threading_length_2);
	var external_threading_length_3 = parseInt($scope.external_threading_length_3);
	var external_threading_length_4 = parseInt($scope.external_threading_length_4);
	var external_threading_length_5 = parseInt($scope.external_threading_length_5);
	
	var external_threading_dia_1 = parseInt($scope.external_threading_dia_1);
    var external_threading_dia_2 = parseInt($scope.external_threading_dia_2);
	var external_threading_dia_3 = parseInt($scope.external_threading_dia_3);
	var external_threading_dia_4 = parseInt($scope.external_threading_dia_4);
	var external_threading_dia_5 = parseInt($scope.external_threading_dia_5);
	
	var external_threading_pitch_1 = parseInt($scope.external_threading_pitch_1);
    var external_threading_pitch_2 = parseInt($scope.external_threading_pitch_2);
	var external_threading_pitch_3 = parseInt($scope.external_threading_pitch_3);
	var external_threading_pitch_4 = parseInt($scope.external_threading_pitch_4);
	var external_threading_pitch_5 = parseInt($scope.external_threading_pitch_5);
	 
	 //kurling
	 
	var knurling_dia_1 = ($scope.knurling_dia_1);
	var knurling_length_1 = ($scope.knurling_length_1);
	
	 
    }
	//Turning
	
	var t_length=$scope.material["t_c"]+length;
	console.log("t_length: "+t_length);
	var rough_t_dia=$scope.material["rough_dia_c"]+dia
	console.log("rough_t_dia: "+rough_t_dia);
	var finish_t_dia=$scope.material["finish_dia_c"]+dia;
	console.log("finish_t_dia: "+finish_t_dia);
	var final_rough_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log("final_rough_rpm: "+final_rough_rpm);
	var final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*finish_t_dia);
	console.log("final_finish_rpm: "+final_finish_rpm);
	var rough_cycle_time = ((t_length)/($scope.material["feed_rough"]*final_rough_rpm))*60*2;
	console.log("rough_cycle_time: "+rough_cycle_time);
	var finish_cycle_time = ((t_length)/($scope.material["feed_finish"]*final_finish_rpm))*60*2;
	console.log("finish_cycle_time: "+finish_cycle_time);
	var total_cycle_time = rough_cycle_time+finish_cycle_time;
	console.log("total_cycle_time: "+total_cycle_time);
    
	//step Turning length calculation 
	if($scope.no_step_turning == 1){
	var step_turning_buffer_1 = ((dia-step_turning_dia_1)/2)/$scope.material["step_buffer"];	
	}
	if($scope.no_step_turning == 2){
		step_turning_length_1 = step_turning_length_1+step_turning_length_2;
		var step_turning_buffer_1 = ((dia-step_turning_dia_1)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_2 = ((step_turning_dia_1-step_turning_dia_2)/2)/$scope.material["step_buffer"];
	}else if($scope.no_step_turning == 3){
		step_turning_length_1 = step_turning_length_1+step_turning_length_2+step_turning_length_3;
		step_turning_length_2 = step_turning_length_2+step_turning_length_3;
		var step_turning_buffer_1 = ((dia-step_turning_dia_1)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_2 = ((step_turning_dia_1-step_turning_dia_2)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_3 = ((step_turning_dia_2-step_turning_dia_3)/2)/$scope.material["step_buffer"];
	}else if($scope.no_step_turning == 4){
		step_turning_length_1 = step_turning_length_1+step_turning_length_2+step_turning_length_3+step_turning_length_4;
		step_turning_length_2 = step_turning_length_2+step_turning_length_3+step_turning_length_4;
		step_turning_length_3 = step_turning_length_3+step_turning_length_4;
		var step_turning_buffer_1 = ((dia-step_turning_dia_1)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_2 = ((step_turning_dia_1-step_turning_dia_2)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_3 = ((step_turning_dia_2-step_turning_dia_3)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_4 = ((step_turning_dia_3-step_turning_dia_4)/2)/$scope.material["step_buffer"];
	}else if($scope.no_step_turning == 5){
		step_turning_length_1 = step_turning_length_1+step_turning_length_2+step_turning_length_3+step_turning_length_4+step_turning_length_5;
		step_turning_length_2 = step_turning_length_2+step_turning_length_3+step_turning_length_4+step_turning_length_5;
		step_turning_length_3 = step_turning_length_3+step_turning_length_4+step_turning_length_5;
		step_turning_length_4 = step_turning_length_4+step_turning_length_5;
		var step_turning_buffer_1 = ((dia-step_turning_dia_1)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_2 = ((step_turning_dia_1-step_turning_dia_2)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_3 = ((step_turning_dia_2-step_turning_dia_3)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_4 = ((step_turning_dia_3-step_turning_dia_4)/2)/$scope.material["step_buffer"];
		var step_turning_buffer_5 = ((step_turning_dia_4-step_turning_dia_5)/2)/$scope.material["step_buffer"];
	}
	//step Turning 1
    
	var step_turning_1_final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*step_turning_dia_1);
    console.log("step_turning_1_final_finish_rpm: "+step_turning_1_final_finish_rpm);
    var step_turning_1_finish_cycle_time = ((step_turning_length_1)/($scope.material["feed_finish"]*step_turning_1_final_finish_rpm))*60*2*step_turning_buffer_1;
    console.log("step_turning_1_finish_cycle_time: "+step_turning_1_finish_cycle_time);
    var step_turning_1_cycle_time = step_turning_1_finish_cycle_time;
    console.log("step_turning_cycle_time: "+step_turning_1_cycle_time);
	
	//step Turning 2
    
    var step_turning_2_final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*step_turning_dia_2);
    console.log("step_turning_2_final_finish_rpm: "+step_turning_2_final_finish_rpm);
    var step_turning_2_finish_cycle_time = ((step_turning_length_2)/($scope.material["feed_finish"]*step_turning_2_final_finish_rpm))*60*2*step_turning_buffer_2;
    console.log("step_turning_2_finish_cycle_time: "+step_turning_2_finish_cycle_time);
    var step_turning_2_cycle_time = step_turning_2_finish_cycle_time;
    console.log("step_turning_cycle_time: "+step_turning_2_cycle_time);

	
	//step Turning 3
    
    var step_turning_3_final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*step_turning_dia_3);
    console.log("step_turning_3_final_finish_rpm: "+step_turning_3_final_finish_rpm);
    var step_turning_3_finish_cycle_time = ((step_turning_length_3)/($scope.material["feed_finish"]*step_turning_3_final_finish_rpm))*60*2*step_turning_buffer_3;
    console.log("step_turning_3_finish_cycle_time: "+step_turning_3_finish_cycle_time);
    var step_turning_3_cycle_time = step_turning_3_finish_cycle_time;
    console.log("step_turning_cycle_time: "+step_turning_3_cycle_time);

	
	//step Turning 4
    
    var step_turning_4_final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*step_turning_dia_4);
    console.log("step_turning_4_final_finish_rpm: "+step_turning_4_final_finish_rpm);
    var step_turning_4_finish_cycle_time = ((step_turning_length_4)/($scope.material["feed_finish"]*step_turning_4_final_finish_rpm))*60*2*step_turning_buffer_4;
    console.log("step_turning_4_finish_cycle_time: "+step_turning_4_finish_cycle_time);
    var step_turning_4_cycle_time = step_turning_4_finish_cycle_time;
    console.log("step_turning_cycle_time: "+step_turning_4_cycle_time);

	
	//step Turning 5
    
    var step_turning_5_final_finish_rpm = ($scope.material["finish_cutting_speed"]*1000)/(3.14*step_turning_dia_5);
    console.log("step_turning_5_final_finish_rpm: "+step_turning_5_final_finish_rpm);
    var step_turning_5_finish_cycle_time = ((step_turning_length_5)/($scope.material["feed_finish"]*step_turning_5_final_finish_rpm))*60*2*step_turning_buffer_5;
    console.log("step_turning_5_finish_cycle_time: "+step_turning_5_finish_cycle_time);
    var step_turning_5_cycle_time = step_turning_5_finish_cycle_time;
    console.log("step_turning_cycle_time: "+step_turning_5_cycle_time);
	
	// calculation for step turning 
	var cycle_time_step_turning ;
	if($scope.no_step_turning == 5)
	{
		cycle_time_step_turning = step_turning_5_cycle_time+step_turning_4_cycle_time+step_turning_3_cycle_time+step_turning_2_cycle_time+step_turning_1_cycle_time;
	}else if($scope.no_step_turning == 4)
	{
		cycle_time_step_turning = step_turning_4_cycle_time+step_turning_3_cycle_time+step_turning_2_cycle_time+step_turning_1_cycle_time;
	}else if($scope.no_step_turning == 3)
	{
		cycle_time_step_turning = step_turning_3_cycle_time+step_turning_2_cycle_time+step_turning_1_cycle_time;
	}else if($scope.no_step_turning == 2)
	{
		cycle_time_step_turning = step_turning_2_cycle_time+step_turning_1_cycle_time;
	}else if($scope.no_step_turning == 1)
	{
		cycle_time_step_turning = step_turning_1_cycle_time;
	}
	//boring
    
	// var boring_constant1=5;
	// var boring_constant2=5;
	// var boring_cot = (-1.21098507444);
	var boring_constant3= 15;
	//boring 1 
	 
	var boring_1_length_input = boring_length_1 ;
	var boring_1_dia_input = boring_dia_1; 
  //  var required_boring_1_length = (boring_1_length_input+boring_constant1+boring_constant2+((boring_1_dia_input/2)*boring_cot)) ;
  var required_boring_1_length = (boring_1_length_input+boring_constant3);
	console.log("required_boring_1_length: "+required_boring_1_length);
	var boring_1_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log($scope.material["rough_cutting_speed"]);
	console.log("boring_1_rpm: "+ boring_1_rpm);
	if(boring_1_dia_input >= 20){
	var boring_1_cycle_time = (required_boring_1_length/(($scope.material["feed_finish"])*boring_1_rpm))*60*2;
	}else{
	var boring_1_cycle_time1 = (required_boring_1_length/(($scope.material["feed_finish"])*boring_1_rpm))*60*2;
	var boring_1_cycle_time2 = (required_boring_1_length/(($scope.material["feed_finish"])*boring_1_rpm))*60*2*(((boring_1_dia_input-20))/2);
	var boring_1_cycle_time =  boring_1_cycle_time1+boring_1_cycle_time2;
	}
	//boring 2 
	 
	var boring_2_length_input = boring_length_2 ;
	var boring_2_dia_input = boring_dia_2 ; 
  //  var required_boring_2_length = (boring_2_length_input+boring_constant1+boring_constant2+((boring_2_dia_input/2)*boring_cot)) ;
	var required_boring_2_length = (boring_2_length_input+boring_constant3);
	console.log("required_boring_2_length: "+required_boring_2_length);
	var boring_2_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log($scope.material["rough_cutting_speed"]);
	console.log("boring_2_rpm: "+ boring_2_rpm);
	if(boring_2_dia_input >= 20){
	var boring_2_cycle_time = (required_boring_2_length/(($scope.material["feed_finish"])*boring_2_rpm))*60*2;
	}else{
	var boring_2_cycle_time1 = (required_boring_2_length/(($scope.material["feed_finish"])*boring_2_rpm))*60*2;
	var boring_2_cycle_time2 = (required_boring_2_length/(($scope.material["feed_finish"])*boring_2_rpm))*60*2*(((boring_2_dia_input-20))/2);
	var boring_2_cycle_time =  boring_2_cycle_time1+boring_2_cycle_time2;
	}
	//boring 3
	 
	var boring_3_length_input = boring_length_3;
	var boring_3_dia_input = boring_dia_3 ; 
    //var required_boring_3_length = (boring_3_length_input+boring_constant1+boring_constant2+((boring_3_dia_input/2)*boring_cot)) ;
	var required_boring_3_length = (boring_3_length_input+boring_constant3);
	console.log("required_boring_3_length: "+required_boring_3_length);
	var boring_3_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log($scope.material["rough_cutting_speed"]);
	console.log("boring_3_rpm: "+ boring_3_rpm);
	if(boring_3_dia_input >= 20){
	var boring_3_cycle_time = (required_boring_3_length/(($scope.material["feed_finish"])*boring_3_rpm))*60*2;
	}else{
	var boring_3_cycle_time1 = (required_boring_3_length/(($scope.material["feed_finish"])*boring_3_rpm))*60*2;
	var boring_3_cycle_time2 = (required_boring_3_length/(($scope.material["feed_finish"])*boring_3_rpm))*60*2*(((boring_3_dia_input-20))/2);
	var boring_3_cycle_time =  boring_3_cycle_time1+boring_3_cycle_time2;
	}
	//boring 4 
	 
	var boring_4_length_input = boring_length_4 ;
	var boring_4_dia_input = boring_dia_4 ; 
    //var required_boring_4_length = (boring_4_length_input+boring_constant1+boring_constant2+((boring_4_dia_input/2)*boring_cot)) ;
	var required_boring_4_length = (boring_4_length_input+boring_constant3);
	console.log("required_boring_4_length: "+required_boring_4_length);
	var boring_4_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log($scope.material["rough_cutting_speed"]);
	console.log("boring_4_rpm: "+ boring_4_rpm);
	if(boring_4_dia_input >= 20){
	var boring_4_cycle_time = (required_boring_4_length/(($scope.material["feed_finish"])*boring_4_rpm))*60*2;
	}else{
	var boring_4_cycle_time1 = (required_boring_4_length/(($scope.material["feed_finish"])*boring_4_rpm))*60*2;
	var boring_4_cycle_time2 = (required_boring_4_length/(($scope.material["feed_finish"])*boring_4_rpm))*60*2*(((boring_4_dia_input-20))/2);
	var boring_4_cycle_time =  boring_4_cycle_time1+boring_4_cycle_time2;
	}
	//boring 5
	 
	var boring_5_length_input = boring_length_5 ;
	var boring_5_dia_input = boring_dia_5 ; 
    //var required_boring_5_length = (boring_5_length_input+boring_constant1+boring_constant2+((boring_5_dia_input/2)*boring_cot)) ;
	var required_boring_5_length = (boring_5_length_input+boring_constant3);
	console.log("required_boring_5_length: "+required_boring_5_length);
	var boring_5_rpm = ($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia);
	console.log($scope.material["rough_cutting_speed"]);
	console.log("boring_5_rpm: "+ boring_5_rpm);
	//var boring_5_cycle_time = (required_boring_5_length/(($scope.material["feed_finish"])*boring_5_rpm))*60;
	if(boring_5_dia_input > 20){
	var boring_5_cycle_time = (required_boring_5_length/(($scope.material["feed_finish"])*boring_5_rpm))*60*2;
	}else{
	var boring_5_cycle_time1 = (required_boring_5_length/(($scope.material["feed_finish"])*boring_5_rpm))*60*2;
	var boring_5_cycle_time2 = (required_boring_5_length/(($scope.material["feed_finish"])*boring_5_rpm))*60*2*(((boring_5_dia_input-20))/2);
	var boring_5_cycle_time =  boring_5_cycle_time1+boring_5_cycle_time2;
	}
	// boring calculation
	if($scope.no_step_boring == 5)
	{
		var total_boring_cycle = boring_5_cycle_time+boring_4_cycle_time+boring_3_cycle_time+boring_2_cycle_time+boring_1_cycle_time;
	}else if($scope.no_step_boring == 4)
	{
		var total_boring_cycle = boring_4_cycle_time+boring_3_cycle_time+boring_2_cycle_time+boring_1_cycle_time;
	}else if($scope.no_step_boring == 3)
	{
		var total_boring_cycle = boring_3_cycle_time+boring_2_cycle_time+boring_1_cycle_time;
	}else if($scope.no_step_boring == 2)
	{
		var total_boring_cycle = boring_2_cycle_time+boring_1_cycle_time;
	}else if($scope.no_step_boring == 1)
	{
		var total_boring_cycle = boring_1_cycle_time;
	} 
	console.log ("boring"+ total_boring_cycle);
	
	//Facing
	 
	var length_of_cut_facing = rough_t_dia/2;
	var facing = (length_of_cut_facing/($scope.material["feed_finish"]*(($scope.material["rough_cutting_speed"]*1000)/(3.14*rough_t_dia))))*60*4;
	console.log(facing);
	if($scope.Internal_threading_req == true)
	 {alert("Internal Threading : "+ $scope.Internal_threading_req );
	//Tapping 
	var tapping_rpm = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*dia);
	console.log(tapping_rpm);
	//Drilling And Tapping 1
	
	var depth_of_hole_1 = Internal_threading_length_1+10;
	console.log(depth_of_hole_1);
	var cycle_time_drilling_1 = (depth_of_hole_1)/($scope.material["feed_finish"]*tapping_rpm)*60;
	console.log(cycle_time_drilling_1);
	var time_for_tapping_1 = (Internal_threading_length_1+(Internal_threading_dia_1/2))/(Internal_threading_pitch_1*tapping_rpm)*60;
	console.log(time_for_tapping_1);
	var time_for_cutting_1 = (((3/2)*(Internal_threading_length_1+(Internal_threading_dia_1/2)))/(Internal_threading_pitch_1*tapping_rpm))*60;
	console.log(time_for_cutting_1);
	var total_cycle_time_internal_threading_1 = cycle_time_drilling_1+time_for_cutting_1+time_for_tapping_1;
	console.log(total_cycle_time_internal_threading_1);
	//Drilling And Tapping 2
	
	var depth_of_hole_2 = Internal_threading_length_2+10;
	console.log(depth_of_hole_2);
	var cycle_time_drilling_2 = (depth_of_hole_2)/($scope.material["feed_finish"]*tapping_rpm)*60;
	console.log(cycle_time_drilling_2);
	var time_for_tapping_2 = (Internal_threading_length_2+(Internal_threading_dia_2/2))/(Internal_threading_pitch_2*tapping_rpm)*60;
	console.log(time_for_tapping_2);
	var time_for_cutting_2 = (((3/2)*(Internal_threading_length_2+(Internal_threading_dia_2/2)))/(Internal_threading_pitch_2*tapping_rpm))*60;
	console.log(time_for_cutting_2);
	var total_cycle_time_internal_threading_2 = cycle_time_drilling_2+time_for_cutting_2+time_for_tapping_2;
    console.log(total_cycle_time_internal_threading_2);
	
	//Drilling And Tapping 3
	
	var depth_of_hole_3 = Internal_threading_length_3+10;
	var cycle_time_drilling_3 = (depth_of_hole_3)/($scope.material["feed_finish"]*tapping_rpm)*60;
	var time_for_tapping_3 = (Internal_threading_length_3+(Internal_threading_dia_3/2))/(Internal_threading_pitch_3*tapping_rpm)*60;
	var time_for_cutting_3 = (((3/2)*(Internal_threading_length_3+(Internal_threading_dia_3/2)))/(Internal_threading_pitch_3*tapping_rpm))*60;
	var total_cycle_time_internal_threading_3 = cycle_time_drilling_3+time_for_cutting_3+time_for_tapping_3;
	console.log(total_cycle_time_internal_threading_3);
	
	//Drilling And Tapping 4
	
	var depth_of_hole_4 = Internal_threading_length_4+10;
	var cycle_time_drilling_4 = (depth_of_hole_4)/($scope.material["feed_finish"]*tapping_rpm)*60;
	var time_for_tapping_4 = (Internal_threading_length_4+(Internal_threading_dia_4/2))/(Internal_threading_pitch_4*tapping_rpm)*60;
	var time_for_cutting_4 = (((3/2)*(Internal_threading_length_4+(Internal_threading_dia_4/2)))/(Internal_threading_pitch_4*tapping_rpm))*60;
	var total_cycle_time_internal_threading_4 = cycle_time_drilling_4+time_for_cutting_4+time_for_tapping_4;
	console.log(total_cycle_time_internal_threading_4);
	
	//Drilling And Tapping 5
	
	var depth_of_hole_5 = Internal_threading_length_5+10;
	var cycle_time_drilling_5 = (depth_of_hole_5)/($scope.material["feed_finish"]*tapping_rpm)*60;
	var time_for_tapping_5 = (Internal_threading_length_5+(Internal_threading_dia_5/2))/(Internal_threading_pitch_5*tapping_rpm)*60;
	var time_for_cutting_5 = (((3/2)*(Internal_threading_length_5+(Internal_threading_dia_5/2)))/(Internal_threading_pitch_5*tapping_rpm))*60;
	var total_cycle_time_internal_threading_5 = cycle_time_drilling_5+time_for_cutting_5+time_for_tapping_5;
    console.log(total_cycle_time_internal_threading_5);
	
	// Cycle Time for Internal Threading
	 
	var no_step_Internal_threading = parseInt($scope.no_step_Internal_threading);
	var cycle_time_internal_threading = 0;	
	if(no_step_Internal_threading == 5)
	{
		var cycle_time_internal_threading = total_cycle_time_internal_threading_5+total_cycle_time_internal_threading_4+total_cycle_time_internal_threading_3+total_cycle_time_internal_threading_2+total_cycle_time_internal_threading_1;
	}else if(no_step_Internal_threading == 4)
	{
		var cycle_time_internal_threading = total_cycle_time_internal_threading_4+total_cycle_time_internal_threading_3+total_cycle_time_internal_threading_2+total_cycle_time_internal_threading_1;
	}else if(no_step_Internal_threading == 3)
	{
		var cycle_time_internal_threading = total_cycle_time_internal_threading_3+total_cycle_time_internal_threading_2+total_cycle_time_internal_threading_1;
	}else if(no_step_Internal_threading == 2)
	{
		var cycle_time_internal_threading = total_cycle_time_internal_threading_2+total_cycle_time_internal_threading_1;
	}else if(no_step_Internal_threading == 1)
	{
		var cycle_time_internal_threading = total_cycle_time_internal_threading_1;
	}
	 }else{
		 var cycle_time_internal_threading = 0 ;
		 alert(" cycle_time_internal_threading : "+  cycle_time_internal_threading );
	 }
	console.log(cycle_time_internal_threading);
	if($scope.external_threading_req == true)
	 {alert("External Threading : "+$scope.external_threading_req );
	//External Threading 1
	
	var threading_rpm_1 = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*external_threading_dia_1);
	console.log(threading_rpm_1);
	var major_dia_1 = (dia-external_threading_dia_1)/2;
	console.log(major_dia_1);
	var cycle_time_to_turn_thread_dia_1 = ((external_threading_length_1)/($scope.material["feed_rough"]*threading_rpm_1)*(major_dia_1*60));
	console.log(cycle_time_to_turn_thread_dia_1);
	var cycle_time_for_threading_1 = (external_threading_length_1/(external_threading_pitch_1*threading_rpm_1))*60;
	console.log(cycle_time_for_threading_1);
	var total_cycle_time_external_threading_1 = cycle_time_for_threading_1+cycle_time_to_turn_thread_dia_1;
	console.log(total_cycle_time_external_threading_1);
	
	//External Threading 2
	
	var threading_rpm_2 = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*external_threading_dia_2);
	var major_dia_2 = (dia-external_threading_dia_2)/2;
	var cycle_time_to_turn_thread_dia_2 = ((external_threading_length_2)/(($scope.material["feed_rough"]*threading_rpm_2)*major_dia_2))*60;
	var cycle_time_for_threading_2 = (external_threading_length_2/(external_threading_pitch_2*threading_rpm_2))*60;
	var total_cycle_time_external_threading_2 = cycle_time_for_threading_2+cycle_time_to_turn_thread_dia_2;
	
	
	//External Threading 3
	
	var threading_rpm_3 = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*external_threading_dia_3);
	var major_dia_3 = (dia-external_threading_dia_3)/2;
	var cycle_time_to_turn_thread_dia_3 = ((external_threading_length_3)/(($scope.material["feed_rough"]*threading_rpm_3)*major_dia_3))*60;
	var cycle_time_for_threading_3 = (external_threading_length_3/(external_threading_pitch_3*threading_rpm_3))*60;
	var total_cycle_time_external_threading_3 = cycle_time_for_threading_3+cycle_time_to_turn_thread_dia_3;
	
	//External Threading 4
	
	var threading_rpm_4 = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*external_threading_dia_4);
	var major_dia_4 = (dia-external_threading_dia_4)/2;
	var cycle_time_to_turn_thread_dia_4 = ((external_threading_length_4)/(($scope.material["feed_rough"]*threading_rpm_4)*major_dia_4))*60;
	var cycle_time_for_threading_4 = (external_threading_length_4/(external_threading_pitch_4*threading_rpm_4))*60;
	var total_cycle_time_external_threading_4 = cycle_time_for_threading_4+cycle_time_to_turn_thread_dia_4;

		
	//External Threading 5
	
	var threading_rpm_5 = (($scope.material["cutting_speed_30percent"])*1000)/(3.14*external_threading_dia_5);
	var major_dia_5 = (dia-external_threading_dia_5)/2;
	var cycle_time_to_turn_thread_dia_5 = ((external_threading_length_5)/(($scope.material["feed_rough"]*threading_rpm_5)*major_dia_5))*60;
	var cycle_time_for_threading_5 = (external_threading_length_5/(external_threading_pitch_5*threading_rpm_5))*60;
	var total_cycle_time_external_threading_5 = cycle_time_for_threading_5+cycle_time_to_turn_thread_dia_5;
    
	//Cycle time External Threading
	
	var no_step_external_threading = $scope.no_step_external_threading;
	if(no_step_external_threading == 5){
		var Cycle_time_external_threading = total_cycle_time_external_threading_5+total_cycle_time_external_threading_4+total_cycle_time_external_threading_3+total_cycle_time_external_threading_2+total_cycle_time_external_threading_1;
	}else if(no_step_external_threading == 4){
		var Cycle_time_external_threading = total_cycle_time_external_threading_4+total_cycle_time_external_threading_3+total_cycle_time_external_threading_2+total_cycle_time_external_threading_1;
	}else if(no_step_external_threading == 3){
		var Cycle_time_external_threading = total_cycle_time_external_threading_3+total_cycle_time_external_threading_2+total_cycle_time_external_threading_1;
	}else if(no_step_external_threading == 2){
		var Cycle_time_external_threading = total_cycle_time_external_threading_2+total_cycle_time_external_threading_1;
	}else if(no_step_external_threading == 1){
		var Cycle_time_external_threading = total_cycle_time_external_threading_1
	}
	 }else{
		 var Cycle_time_external_threading = 0 ;
		 alert("Cycle_time_external_threading : "+ Cycle_time_external_threading);
	 }
	
	// kurling
	
	var kurling_rpm = ($scope.material["cutting_speed_30percent"]*1000)/(3.14*knurling_dia_1);
	console.log("kurling_rpm : "+ kurling_rpm);
	var kurling_cycle_time = (knurling_length_1/($scope.material["feed_finish"]*kurling_rpm))*60*2;
	console.log("kurling_cycle_time"+kurling_cycle_time);
	
	//material cost
	var diaa = parseInt(rough_t_dia);
	var material_cost = (((3.14/4)*(Math.pow(diaa, 2))*t_length*$scope.material["density"])/1000000)*($scope.material["rate"]/3600);
	
	
	//total cycle time 
	
	
	var total = total_cycle_time+cycle_time_step_turning+total_boring_cycle+facing+cycle_time_internal_threading+Cycle_time_external_threading;
	
	// Labour cost 
	
	var labour = total * (($scope.material["machinerate"])/3600);
	var labour_cost = labour;
	//surface Area 
	
	//Turning 
	var sa_turning;
	if($scope.turning_req == false)
	{
	 sa_turning = 0;
	}else
	{
	 sa_turning = 3.14*dia*length;
	}
	console.log("sa_turning  : "+sa_turning);
	
	//boring
	var sa_boring1 = 3.14*boring_length_1*boring_dia_1;
	var sa_boring2 = 3.14*boring_length_2*boring_dia_2;
	var sa_boring3 = 3.14*boring_length_3*boring_dia_3;
	var sa_boring4 = 3.14*boring_length_4*boring_dia_4;
	var sa_boring5 = 3.14*boring_length_5*boring_dia_5;
	var sa_boring;
	if($scope.boring_req == false){
		 sa_boring  = 0;
	}else if($scope.no_step_boring == 5){
	 sa_boring  = sa_boring1+sa_boring2+sa_boring3+sa_boring4+sa_boring5;
	}else if($scope.no_step_boring == 4){
	 sa_boring  = sa_boring1+sa_boring2+sa_boring3+sa_boring4;
	}else if($scope.no_step_boring == 3){
	 sa_boring  = sa_boring1+sa_boring2+sa_boring3;
	}else if($scope.no_step_boring == 2){
	 sa_boring  = sa_boring1+sa_boring2;
	}else if($scope.no_step_boring == 1){
	 sa_boring  = sa_boring1;
	} 
	console.log("sa_boring"+sa_boring);
	
	
	//Facing
    var sa_facing;
	if($scope.turning_req == true)
	{
	sa_facing = 2*3.14*(length/2)*(dia/2);
	}else{
	sa_facing = 0;	
	}
	console.log("sa_facing"+sa_facing);
	
	// Tapping
	var sa_tapping1 = 3.14*Internal_threading_length_1*Internal_threading_dia_1;
	var sa_tapping2 = 3.14*Internal_threading_length_2*Internal_threading_dia_2;
	var sa_tapping3 = 3.14*Internal_threading_length_3*Internal_threading_dia_3;
	var sa_tapping4 = 3.14*Internal_threading_length_4*Internal_threading_dia_4;
	var sa_tapping5 = 3.14*Internal_threading_length_5*Internal_threading_dia_5;
	
	// calculation tapping
	if($scope.Internal_threading_req == false)
	{
	var sa_tapping = 0;	
	}else if($scope.no_step_Internal_threading == 5){
	var sa_tapping  = sa_tapping1+sa_tapping2+sa_tapping3+sa_tapping4+sa_tapping5;
	}else if($scope.no_step_Internal_threading == 4){
	var sa_tapping  = sa_tapping1+sa_tapping2+sa_tapping3+sa_tapping4;
	}else if($scope.no_step_Internal_threading == 3){
	var sa_tapping  = sa_tapping1+sa_tapping2+sa_tapping3;
	}else if($scope.no_step_Internal_threading == 2){
	var sa_tapping  = sa_tapping1+sa_tapping2;
	}else if($scope.no_step_Internal_threading == 1){
	var sa_tapping  = sa_tapping1;
	} 
	
	//Threading
	var sa_threading1 = 3.14*external_threading_length_1*external_threading_dia_1;
	var sa_threading2 = 3.14*external_threading_length_2*external_threading_dia_2;
	var sa_threading3 = 3.14*external_threading_length_3*external_threading_dia_3;
	var sa_threading4 = 3.14*external_threading_length_4*external_threading_dia_4;
	var sa_threading5 = 3.14*external_threading_length_5*external_threading_dia_5;
	
	// calculation Threading
	if($scope.external_threading_req == false){
		var sa_threading  = 0 ;
	}else if($scope.no_step_external_threading == 5){
	var sa_threading  = sa_threading1+sa_threading2+sa_threading3+sa_threading4+sa_threading5;
	}else if($scope.no_step_external_threading == 4){
	var sa_threading  = sa_threading1+sa_threading2+sa_threading3+sa_threading4;
	}else if($scope.no_step_external_threading == 3){
	var sa_threading  = sa_threading1+sa_threading2+sa_threading3;
	}else if($scope.no_step_external_threading == 2){
	var sa_threading  = sa_threading1+sa_threading2;
	}else if($scope.no_step_external_threading == 1){
	var sa_threading  = sa_threading1;
	} 
	
	// Total SUrface Area
	 var total_SurfaceArea = sa_threading+sa_tapping+sa_facing+sa_boring+sa_turning;
	 console.log("total_SurfaceArea"+total_SurfaceArea);
	 
	 //Surface Area in Sq INCH
	 var sufaceArea_sq_inch = total_SurfaceArea/(25.4*25.4);
	 console.log("sufaceArea_sq_inch"+sufaceArea_sq_inch);
	 
	//weight calculation 
	var	majordia_length_wt
	if($scope.turning_req == false){
	majordia_length_wt = 0;
	}else
	{
	majordia_length_wt =  (((3.14/4)*(Math.pow(diaa, 2))*t_length*$scope.material["density"])/1000000);
	console.log("majordia_length_wt"+majordia_length_wt);
	}
	var step_turning_wt;
	if($scope.step_turning_req == false ){
		step_turning_wt = 0;
	}else{
		
	var step_turning_1_wt = (((3.14/4)*(Math.pow(step_turning_dia_1,2))*step_turning_length_1*$scope.material["density"])/1000000);
	var step_turning_2_wt = (((3.14/4)*(Math.pow(step_turning_dia_2,2))*step_turning_length_2*$scope.material["density"])/1000000);
	var step_turning_3_wt = (((3.14/4)*(Math.pow(step_turning_dia_3,2))*step_turning_length_3*$scope.material["density"])/1000000);
	var step_turning_4_wt = (((3.14/4)*(Math.pow(step_turning_dia_4,2))*step_turning_length_4*$scope.material["density"])/1000000);
	var step_turning_5_wt = (((3.14/4)*(Math.pow(step_turning_dia_5,2))*step_turning_length_5*$scope.material["density"])/1000000);
	if($scope.no_step_turning == 5)
	{
	  step_turning_wt = step_turning_1_wt-step_turning_2_wt-step_turning_3_wt-step_turning_4_wt-step_turning_5_wt;
	}else if($scope.no_step_turning == 4)
	{
	  step_turning_wt = step_turning_1_wt-step_turning_2_wt-step_turning_3_wt-step_turning_4_wt;
	}else if($scope.no_step_turning == 3)
	{
	  step_turning_wt = step_turning_1_wt-step_turning_2_wt-step_turning_3_wt;
	}else if($scope.no_step_turning == 2)
	{
	  step_turning_wt = step_turning_1_wt-step_turning_2_wt;
	}else if($scope.no_step_turning == 1)
	{
	  step_turning_wt = step_turning_1_wt;
	}
	}
	console.log("step_turning_wt"+step_turning_wt);
	var boring_wt;
	if($scope.boring_req == false){
		boring_wt = 0;
	}else{
	var boring_1_wt = (((3.14/4)*(Math.pow(boring_dia_1,2))*boring_length_1*$scope.material["density"])/1000000);
	var boring_2_wt = (((3.14/4)*(Math.pow(boring_dia_2,2))*boring_length_2*$scope.material["density"])/1000000);
	var boring_3_wt = (((3.14/4)*(Math.pow(boring_dia_3,2))*boring_length_3*$scope.material["density"])/1000000);
	var boring_4_wt = (((3.14/4)*(Math.pow(boring_dia_4,2))*boring_length_4*$scope.material["density"])/1000000);
    var boring_5_wt = (((3.14/4)*(Math.pow(boring_dia_5,2))*boring_length_5*$scope.material["density"])/1000000);
	if($scope.no_step_boring == 5){
		boring_wt = boring_1_wt-boring_2_wt-boring_3_wt-boring_4_wt-boring_5_wt;
	}else if($scope.no_step_boring == 4){
		boring_wt = boring_1_wt-boring_2_wt-boring_3_wt-boring_4_wt;
	}else if($scope.no_step_boring == 3){
		boring_wt = boring_1_wt-boring_2_wt-boring_3_wt;
	}else if($scope.no_step_boring == 2){
		boring_wt = boring_1_wt-boring_2_wt;
	}else if($scope.no_step_boring == 1){
		boring_wt = boring_1_wt;
	}	    		
	}
	console.log("boring_wt"+boring_wt);
	
	var final_wt = majordia_length_wt-step_turning_wt-boring_wt;
	console.log("final_wt"+final_wt);
	
	var final_wt_inGrams = final_wt*1000;
	console.log("final_wt_inGrams"+final_wt_inGrams);
	
	var final_wt_30percent = final_wt_inGrams*(30/100);
	console.log("final_wt_30percent"+final_wt_30percent);
	var wt_per = final_wt/1000;
	console.log("wt_per"+wt_per);
	//var final_wt_per = Math.round(wt_per);
	var final_wt_per = wt_per;
	
	console.log("final_wt_per"+final_wt_per);
	
	alert(labour_cost);
	//price on finshwt
	var priceonfinshwt ;
	if(final_wt_30percent >=0 && final_wt_30percent<51){
	var priceonfinshwt = labour_cost*$scope.wt["0"];
	}else	if(final_wt_30percent >=51 && final_wt_30percent<101){
	var priceonfinshwt = labour_cost*$scope.wt["51"];
	}else	if(final_wt_30percent >=101 && final_wt_30percent<501){
	var priceonfinshwt = labour_cost*$scope.wt["101"];
	}else	if(final_wt_30percent >=501 && final_wt_30percent<1001){
	var priceonfinshwt = labour_cost*$scope.wt["501"];
	}else	if(final_wt_30percent >=1001){
	var priceonfinshwt = labour_cost*$scope.wt["1001"];
	}
	console.log("priceonfinshwt"+priceonfinshwt);
	// price on Quantity
	var Quantity = parseInt($scope.Quantity);
	alert(Quantity);
	var wtperQty = 0;
	if(Quantity >=100 && Quantity<301)
	{
		
		var wtperQty = priceonfinshwt*$scope.qt["100"];
		
	}else if(Quantity>=301 && Quantity<501)
	{
		var wtperQty = priceonfinshwt*$scope.qt["301"];
	}else if(Quantity>=501 && Quantity<1001)
	{
		var wtperQty = priceonfinshwt*$scope.qt["501"];
	}else if(Quantity>=1001 && Quantity<3001)
	{
		var wtperQty = priceonfinshwt*$scope.qt["1001"];
	}else if(Quantity>=3001)
	{
		var wtperQty = priceonfinshwt*$scope.qt["3001"];
	} 
	console.log("wtperQty"+wtperQty);
	var material_Labour = wtperQty*material_cost;
	console.log("material_Labour"+material_Labour);
	
	//final_wt_30percent
	//Additional opertation 

	
	var Anodising_rate = 2;
	var ENP_rate = 1.4;
	var Chrome_Plating_rate = 8; 
	var Nickel_Plating_rate = 1;
	var Zinc_Plating_rate = 60;
	var Chemical_Black_rate = 20;
	var Heat_treatment_rate = 40;
	var Case_Hardening_rate = 60;
	var Blasting_rate = 2;
	var Passivation_rate = 5;
	var Powder_coating_rate = 10;
	var Laser_Engraving_rate = 5;
	var Tumbling_rate = 50;
	var Spray_Painting_rate = 10;	
	
	var Anodising =   Anodising_rate*sufaceArea_sq_inch;
	var ENP = ENP_rate*sufaceArea_sq_inch;
	var Chrome_Plating = Chrome_Plating_rate*sufaceArea_sq_inch;
	var Nickel_Plating = Nickel_Plating_rate*sufaceArea_sq_inch;
	var Zinc_Plating = (final_wt_30percent/1000)*Zinc_Plating_rate;
	var Chemical_Black = (final_wt_30percent/1000)*Chemical_Black_rate;
	var Heat_treatment = (final_wt_30percent/1000)*Heat_treatment_rate;
	var Case_Hardening = (final_wt_30percent/1000)*Case_Hardening_rate;
	var Blasting = Blasting_rate*sufaceArea_sq_inch;
	var Passivation = Passivation_rate*sufaceArea_sq_inch;
	var Powder_coating = Powder_coating_rate*sufaceArea_sq_inch;
	var Laser_Engraving = Laser_Engraving_rate*sufaceArea_sq_inch;
	var Tumbling = (final_wt_30percent/1000)*Tumbling_rate;
	var Spray_Painting = Spray_Painting_rate*sufaceArea_sq_inch;
		
		var additional_cost;
	if($scope.Anodising){
		additional_cost = additional_cost+Anodising;
	}
	if($scope.ENP){
		additional_cost = additional_cost+ENP;
	}if($scope.Chrome_Plating){
		additional_cost = additional_cost + Chrome_Plating;
	}if($scope.Nickel_Plating){
		additional_cost = additional_cost+ Nickel_Plating;
	}if($scope.Zinc_Plating){
		additional_cost = additional_cost+Zinc_Plating;
	}if($scope.Chemical_Black){
		additional_cost = additional_cost+Chemical_Black;
	}if($scope.Heat_treatment){
		additional_cost = additional_cost+Heat_treatment;
	}if($scope.Case_Hardening){
		additional_cost = additional_cost+Case_Hardening;
	}if($scope.Blasting){
		additional_cost = additional_cost+Blasting;
	}if($scope.Passivation){
		additional_cost = additional_cost+Passivation;
	}if($scope.Powder_coating){
		additional_cost = additional_cost+Powder_coating;
	}if($scope.Laser_Engraving){
		additional_cost = additional_cost+Laser_Engraving;
	}if($scope.Tumbling){
		additional_cost = additional_cost+Tumbling;
	}if($scope.Spray_Painting){
		additional_cost = additional_cost=Spray_Painting;
	}
	console.log("additional_cost"+additional_cost);
	
	
	
	
	//$scope.turningcycle = total_cycle_time;
    //$scope.stepturningcycle = cycle_time_step_turning;
	//$scope.boringcycle = total_boring_cycle;
	//$scope.facingcycle = facing;
	//$scope.kurlingcycle = kurling_cycle_time;
	//$scope.Internalthreading = cycle_time_internal_threading;
	//$scope.extenalthreading = Cycle_time_external_threading;
	//$scope.material_wt = majordia_length_wt;
	$scope.material_cost = material_cost;
	$scope.machine_cost = total_cycle_time*($scope.material["rate"]/3600);
	$scope.labour_cost = labour_cost;
	$scope.additional_cost = additional_cost;
	
}	
	  });
	
