'use strict';

app.config(function ($stateProvider) {
       $stateProvider
            .state('newrfq', {
               // parent: 'buyerdashboard',
                url: '/NewRFQ',
                // data: {
                    // authorities: []
                // },
                views: {
					   
               'header@': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar@': { templateUrl: 'scripts/app/particles/navbar.html'},
				'content@': {
					templateUrl: 'scripts/app/buyer/rfq/createnewrfq.html',
					controller: 'ezeerfqCtrl'
					
				}
			}
            })
			   .state('rfqhistory', {
            //  parent: 'buyerdashboard',
                url: '/RFQList',
                // data: {
                    // authorities: []
                // },
                views: {
					   
               'header@': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar@': { templateUrl: 'scripts/app/particles/navbar.html'},
				'content@': {
					templateUrl: 'scripts/app/buyer/rfq/rfqhistory.html',
					controller: 'ezeerfqhistoryCtrl'
					
				}
			}
            });


    });
