'use strict';

app.config(function ($stateProvider) {
       $stateProvider
            .state('contractlist', {
               // parent: 'buyerdashboard',
                url: '/ContractList',
                // data: {
                    // authorities: []
                // },
                views: {
					   
               'header@': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar@': { templateUrl: 'scripts/app/particles/navbar.html'},
				'content@': {
					templateUrl: 'scripts/app/buyer/order/orderlist.html',
					controller: 'ezeeorderCtrl'
					
				}
			}
            })
			.state('contractstatus', {
            //  parent: 'buyerdashboard',
                url: '/ContractStatus',
                // data: {
                    // authorities: []
                // },
                views: {
					   
               'header@': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar@': { templateUrl: 'scripts/app/particles/navbar.html'},
				'content@': {
					templateUrl: 'scripts/app/buyer/order/orderstatus.html',
					controller: 'ezeeorderstatusCtrl'
					
				}
			}
            })
			   .state('contracthistory', {
            //  parent: 'buyerdashboard',
                url: '/ContractHistory',
                // data: {
                    // authorities: []
                // },
                views: {
					   
               'header@': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar@': { templateUrl: 'scripts/app/particles/navbar.html'},
				'content@': {
					templateUrl: 'scripts/app/buyer/order/orderhistory.html',
					controller: 'ezeeorderhistoryCtrl'
					
				}
			}
            });


    });
