'use strict';

var app = angular.module(
		'ezeeApp',
		[ 'LocalStorageModule', 'ngCookies', 'ngAria',
				'ngCacheBuster', 'ngMaterial', 'ngAnimate',
				'ngMessages',
				'ui.bootstrap', 'ui.router' ])
app.config(function($mdIconProvider) {
    $mdIconProvider
       .iconSet('social', 'img/icons/sets/social-icons.svg', 24)
       .defaultIconSet('img/icons/sets/core-icons.svg', 24);
   })				
app.config(function($stateProvider, $urlRouterProvider) {		
$urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            views: {
                
                'header': { templateUrl: 'scripts/app/account/homepage.html' ,
                 controller: 'accountController'
				 }

            }
            
        }) 	
        
        
        
        // Buyer Account PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('buyerdashboard', {
            url: '/dashboard',
            views: {
                
               'header': { templateUrl: 'scripts/app/particles/topbar.html' },
               'navbar': { templateUrl: 'scripts/app/particles/navbar.html'
			    // 'header': { template: 'scripts/app/particles/topbar.html' },
                // 'navbar': { template: 'scripts/app/particles/navbar.html'
                   // templateUrl: 'table-data.html',
                    //controller: 'scotchController'
                },
				'content':{templateUrl: 'scripts/app/buyer/dashboard/dashboard.html', 
				   controller: 'buyerdashboardCtrl'
				 }
            }
            
        });
        
});
		
	
		
			
